#### **Установка Zabbix в Docker на Raspberry Pi.**

Добрый день, коллеги!

Сегодня я хочу поделиться с Вами своим опытом установки приложений в контейнерах Docker на Raspberry Pi.

Появилась необходимость установить Zabbix для тестирования и экспериментов. Zabbix должен быть всегда включен, поэтому установка на ноутбук или стационарный компьютер не подходит. В моем распоряжении был микрокомпьютер  Raspberry Pi. Он хорошо подходит для небольших проектов и там, где нет возможности держать включенным компьютер постоянно и нет гипервизора с виртуальными машинами. И контейнеры - это хорошая альтернатива виртуальным машинам.



В проекте использовались:
```
    1) Аппаратное обеспечение - Raspberry Pi v3 B, архитектура CPU - armv7l

    2) Операционная система - GNU\Linux Raspbian 10. (Будем считать, что Вы умеете работать в командной строке и подключаться по SSH.)

```
Дла начала установим Portainer - веб-интерфейс для управления docker-контейнерами. Бесплатно, удобно, подойдет новичкам в docker. 



Установка Portainer:
```
    1) docker volume create portainer_data

    2) docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```
После установки веб-интерфейс Portainer будет открыт на http://ip-адрес-вашего-хоста:9000

Теперь скачаем и запустим контейнеры. Но не тут-то было. 
 
Первое, с чем я столкнулся - отсутствие контейнеров с My Sql для ARM архитектур. 

Поэтому идем на сайт https://hub.docker.com и ищем необходимое. Я воспользовался вот этим https://hub.docker.com/r/biarms/mysql


Сразу стоит обратить внимание на сетевые подключения контейнеров. Шлюз моста займет адрес Х.Х.Х.1, контейнер с Portainer-ом получит адрес Х.Х.Х.2 и так далее. Это будет иметь значение при подключении приложений в контейнерах.


Установка контейнера с My SQL:
```
    1) docker pull biarms/mysql

    2) docker run -d -p 3306:3306 --name mysql -t -e MYSQL_DATABASE="zabbix" -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="zabbix" -e MYSQL_ROOT_PASSWORD="root" -d biarms/mysql:latest

    3) docker logs mysql-server
```


Установка контейнера с Zabbix - Server:
```
    1) docker network inspect bridge

    2) docker run --name zabbix-server -t -p 10051:10051 -e DB_SERVER_HOST="172.17.0.3" -e MYSQL_DATABASE="zabbix" -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="zabbix" -e MYSQL_ROOT_PASSWORD="root" --link mysql:mysql -d zabbix/zabbix-server-mysql:latest

    3) docker logs zabbix-server

```

Проверяем подключение к My SQL со стороны Zabbix Server-а:
```
    1) docker exec -it zabbix-server bash

    2) mysql -u zabbix -p zabbix -h 172.17.0.3
```


Установка контейнера с WEB-интерфейсом Zabbix:
```
    1) docker run --name zabbix-web -t -p 54321:54321 -e DB_SERVER_HOST="172.17.0.3" -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="zabbix" -e ZBX_SERVER_HOST="172.17.0.4" -e ZBX_SERVER_NAME="Zabbix" -e PHP_TZ="Europe/Moscow" -d zabbix/zabbix-web-apache-mysql:latest

    2) docker logs zabbix-web

```
IPTABLES
```
1) sudo iptables -t nat -A PREROUTING -p tcp -d 192.168.1.12 --dport 8080 -j DNAT --to-destination 172.17.0.5:8080

2) sudo iptables -A FORWARD -i enx00009daa0000 -d 172.17.0.5 -p tcp --dport 8080 -j ACCEPT
```

Выполняем первый вход в веб-интерфейс сервера Zabbix:
http://ip_address
УЗ по умолчанию: admin / zabbix


Использованные материалы:
https://hub.docker.com/r/biarms/mysql
https://mandrake.su/2019/06/12/ustanovka-zabbix-v-docker/
https://www.djack.com.pl/
