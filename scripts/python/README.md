#### Installed Zabbix-agent on Free-PBX.

- Zabbix Server - ver. 4.x

- Zabbix Agent - ver. 4.x

- Free-PBX - ver. 14.x, powered by CentOS 7

- Python - ver. 3.x

##### Procedure of actions.

1) Check kernel version on PBX-distro. Kernel version must be 3.x or latter.

2) Install zabbix-agent.

3) Download archive on your local PC and install bash-scripts on PBX.

4) Setup monitoring PBX-hosts on Zabbix Server. 


Manuals: 

https://voxlink.ru/kb/linux/nastrojka-zabbix-agenta-dlja-monitoringa-dannyh-ats-skripty/

https://serveradmin.ru/monitoring-asterisk-v-zabbix/

Install zabbix-agent on CentOS 6:

https://tecadmin.net/install-zabbix-agent-on-centos-rhel/
