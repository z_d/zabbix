import paramiko, os

look_for_keys=False
allow_agent=False

hosts = {
 'x.x.x.x': 'Hostname', 
 'y.y.y.y': 'Hostname',  
}

for host in hosts:
	port = 22
	transport = paramiko.Transport((host, port))
	transport.connect(username='root', password='Password')
	sftp = paramiko.SFTPClient.from_transport(transport)
	
	sftp.put('C:\\Users\\user\\Desktop\\bash_script_freepbx\\bash_script_freepbx.zip', '/tmp/bash_script_freepbx.zip')
	sftp.close()
	transport.close()
	print('Done!')
	
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(hostname=host, username='root', password='Password', port=22)
	stdin, stdout, stderr = client.exec_command('mkdir -p /etc/zabbix/scripts && \
	                                             unzip /tmp/bash_script_freepbx.zip -d /etc/zabbix/scripts/ && \
	                                             rm -f /tmp/bash_script_freepbx.zip && \
	                                             bash /etc/zabbix/scripts/user_parameter.sh && \
	                                             cd /etc/zabbix && \
	                                             chown -R zabbix. scripts/ && \
	                                             chmod 755 -R scripts/  && \
	                                             echo "zabbix ALL = NOPASSWD: /usr/sbin/asterisk" >> /etc/sudoers ')
	data = stdout.read() + stderr.read()
	content = data.decode('utf-8')
	print(content, host)
	print('Done!')
	client.close()
