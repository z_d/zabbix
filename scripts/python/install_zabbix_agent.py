import paramiko

look_for_keys=False
allow_agent=False


hosts = {
  'y.y.y.y': 'hostname',
  'x.x.x.x': 'hostname', 
}

for host in hosts:
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(hostname=host, username='root', password='Password', port=22)
	stdin, stdout, stderr = client.exec_command("rpm -Uvh https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-2.el7.noarch.rpm && \
                                                                                                                             yum clean all && \
                                                                                                                             yum install zabbix-agent -y && \
                                                                                                                             systemctl enable zabbix-agent && \
                                                                                                                             systemctl start zabbix-agent && \
                                                                                                                             sed -i 's/127.0.0.1$/ip-addr-zabbix-server/g' /etc/zabbix/zabbix_agentd.conf && \
                                                                                                                             sed -i 's|Zabbix server$|Hostname|g' /etc/zabbix/zabbix_agentd.conf && \
                                                                                                                             systemctl restart zabbix-agent")
	data = stdout.read() + stderr.read()
	content = data.decode('utf-8')
	print(content)
	print('Done!', hosts[host], host)
	client.close()
