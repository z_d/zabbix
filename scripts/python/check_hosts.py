import paramiko

look_for_keys=False
allow_agent=False

hosts = {
'x.x.x.x': 'hostname',
'y.y.y.y': 'hostname',
}

#print(*hosts.keys(), sep='\n')

for key in hosts:
	host=key
	client = paramiko.SSHClient()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(hostname=host, username='root', password='password', port=22)
	stdin, stdout, stderr = client.exec_command("uname -r")
	data = stdout.read() + stderr.read()
	print(data, host)
	client.close()
