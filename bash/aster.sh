#!/bin/sh
aster=`/etc/init.d/asterisk status`
string=`expr "$aster" : '.*\(not running\).*'`
if [ "$string" = "not running" ]
then
	echo 0
else
	echo 1
fi
