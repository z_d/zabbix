#!/bin/sh
proc=`/usr/bin/sudo /usr/bin/top -bn1 | grep Cpu | awk '{print $5}'`
proc=${proc%%\%id,}
echo $(echo 100 $proc | awk '{print $1-$2}')
