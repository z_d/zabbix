#!/bin/sh
number_tranks=`/usr/bin/sudo /usr/sbin/asterisk -rx "sip show registry" | grep "SIP registrations" | awk '{print $1}'`
reg_tranks=`/usr/bin/sudo /usr/sbin/asterisk -rx "sip show registry" | grep Registered | wc -l`
let result=$number_tranks-$reg_tranks
echo $result
