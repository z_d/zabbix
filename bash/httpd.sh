#!/bin/sh
httpd=`/usr/bin/sudo /etc/init.d/httpd status`
string=`expr "$httpd" : '.*\(not running\).*'`
if [ "$string" = "not running" ]
then
	echo 0
else
	echo 1
fi
