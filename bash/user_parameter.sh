#!/bin/bash

echo 'UserParameter=trunks,/etc/zabbix/scripts/trunks.sh' >> /etc/zabbix/zabbix_agentd.conf && \
echo 'UserParameter=free,/etc/zabbix/scripts/free.sh' >> /etc/zabbix/zabbix_agentd.conf && \
echo 'UserParameter=proc,/etc/zabbix/scripts/proc.sh' >> /etc/zabbix/zabbix_agentd.conf && \
echo 'UserParameter=mysql,/etc/zabbix/scripts/mysql.sh' >> /etc/zabbix/zabbix_agentd.conf && \
echo 'UserParameter=httpd,/etc/zabbix/scripts/httpd.sh' >> /etc/zabbix/zabbix_agentd.conf && \
echo 'UserParameter=aster,/etc/zabbix/scripts/aster.sh' >> /etc/zabbix/zabbix_agentd.conf && \
echo 'UserParameter=notok,/etc/zabbix/scripts/notok.sh' >> /etc/zabbix/zabbix_agentd.conf && \
echo 'UserParameter=peers,/etc/zabbix/scripts/peers.sh' >> /etc/zabbix/zabbix_agentd.conf && \
echo 'UserParameter=pbx,/etc/zabbix/scripts/pbx.sh' >> /etc/zabbix/zabbix_agentd.conf && \


systemctl restart zabbix-agent

exit 0
