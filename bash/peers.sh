#!/bin/sh
ok=`/usr/bin/sudo asterisk -rx 'sip show peers' | grep -E "^\<[0-9]{4}\>" | grep -c OK`
notok=`asterisk -rx 'sip show peers' | grep -E "^\<[0-9]{4}\>" | grep -vc OK`
echo $ok/$notok
